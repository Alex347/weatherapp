let express = require('express');
let request = require('request-promise');
let mongoose = require('mongoose');
// DB config
const db = require("./config/keys").mongoURI;

let app = express();
app.set('view engine', 'ejs');

//connect to mongodb
mongoose
.connect(db,{ useNewUrlParser: true }
)
.then(() => console.log("MongoDb Connected"))
.catch(err => console.log(err));

let citySchema = new mongoose.Schema({
name:String
});
let cityModel = mongoose.model('City',citySchema);

//test city
// let lasvegas = new cityModel({name:'Las Vegas'});
// let toronto = new cityModel({name:'Toronto'});
// let montpellier = new cityModel({name:'Montpellier'});

//save cities
// lasvegas.save();
// toronto.save();
// montpellier.save();


const getWeather= async (cities)=>{
    let weather_data = [];
    for(let city_obj of cities){

        let city = city_obj.name;

        let url= `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=imperial&APPID=b7ef78bb7476ea8f2850ec42c29a5ed6`;
       
        let response_body = await request(url);

        let weather_json = JSON.parse(response_body);

        let weather = {
                        city: city ,
                        temperature: Math.round(weather_json.main.temp) ,
                        description: weather_json.weather[0].description,
                        icon: weather_json.weather[0].icon,
                    
                    }
                    weather_data.push(weather);
    }return weather_data;
}



app.get('/', (req,res)=>{
    cityModel.find({},(err,cities)=>{

        getWeather(cities).then((results)=>{
            console.log(results);

            let weather_data = {weather_data : results};
            res.render('weather',weather_data);
        });
    })

    let weather_data = {weather : weather};
    });
    


app.listen(3000);